package com.company;

import java.util.Random;

public class Main {

    public static void main(String[] args) {

        Random random = new Random();
        random.nextInt(2);

        //Declaring a constant
        int SIZE = 50;

        char zero = ' ';
        char one = 'X';

        //Declaring an array
        int[][] game;
        //Defining an array
        game = new int[SIZE][SIZE];

        //Filling an array
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                game[i][j] = random.nextInt(2);
            }
        }


        //Array Output
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                if (game[i][j] == 0)
                    System.out.print(zero);
                else System.out.print(one);
            }
            System.out.println();
        }


    }
}
